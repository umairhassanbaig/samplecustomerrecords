//
//  CustomerRecordsPresenter.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/13/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit
import CoreLocation

protocol CustomerRecordsViewable: class {
    func showLoading()
    func hideLoading()
    func set(customers: [CustomerRecordViewModel])
    func showError(error: Error)
}


class CustomerRecordsPresenter {

    public var maxDistance: Double = kDISTANCE_CUSTOMER_MAX
    public var currentCoordinates : Coordinates = kCOORDINATES_OFFICE
    private let interactor: CustomerInteractor
    private weak var view: CustomerRecordsViewable?
    
    init() {
        self.interactor = CustomerInteractor()
        self.interactor.delegate = self
    }
    
    
    public func attach(view: CustomerRecordsViewable) {
        self.view = view
    }
    
    public func getNearby() {
        view?.showLoading()
        interactor.getCustomRecords()
    }
    
    private func generateViewModels(customers: [Customer]) -> [CustomerRecordViewModel] {
        
        var result = [CustomerRecordViewModel]()
        for customer in customers {
            
            //Check for coordinates validity.
            guard let dLat = Double(customer.latitude), let dLong = Double(customer.longitude) else {
                continue
            }
            guard CLLocationCoordinate2DIsValid(CLLocationCoordinate2D(latitude: dLat, longitude: dLong)) else {
                continue
            }
            
            //Calculation distance
            let distance = SphereHelper.orthodromicDistance(
                from: self.currentCoordinates,
                to: Coordinates(latitude:dLat, longitude: dLong),
                sphereRadius: kRADIUS_EARTH)
            
            //Check if in range
            if distance <= maxDistance {
                let viewModel = CustomerRecordViewModel(customer: customer, distance: distance)
                result.append(viewModel)
            }
    
        }
        result.sort(by: {$0.user_id < $1.user_id})
        return result
    }
    
    
}


extension CustomerRecordsPresenter : CustomerInteractorDelegate {
    
    func received(customers: [Customer]) {
        //Using background thread: to handle large files
        DispatchQueue.global(qos: .background).async {
            let viewModels = self.generateViewModels(customers: customers)
            
            DispatchQueue.main.sync {
                self.view?.hideLoading()
                self.view?.set(customers: viewModels)
            }
        }
    }
    
    
    func failedToReceiveCustomers(error: Error) {
        view?.hideLoading()
        view?.showError(error: error)
    }
    
}


class CustomerRecordViewModel {
    
    let user_id : Int
    let name: String
    let distance: Double
    init(customer: Customer, distance: Double) {
        self.user_id = customer.user_id
        self.name = customer.name
        self.distance = distance
    }
    
}
