//
//  CustomerCell.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/13/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

class CustomerCell: UITableViewCell {

    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!

    var customerViewModel: CustomerRecordViewModel! {
        didSet {
            lblNumber.text = String(customerViewModel.user_id)
            lblTitle.text = customerViewModel.name
            lblSubtitle.text =  String(format: "%.1f km away",  customerViewModel.distance)
            
        }
    }
}
