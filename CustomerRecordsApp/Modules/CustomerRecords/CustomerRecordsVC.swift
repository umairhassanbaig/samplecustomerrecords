//
//  CustomerRecordsVC.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/13/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

class CustomerRecordsVC: UIViewController {

    var presenter = CustomerRecordsPresenter()
    
    fileprivate let kCELLID_CUSTOMER = "kCELLID_CUSTOMER"
    fileprivate var dataSource = [CustomerRecordViewModel]()
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var lblNodata: UILabel!
    fileprivate lazy var refreshControl : UIRefreshControl = {
        let refreshController = UIRefreshControl()
        refreshController.addTarget(self, action: #selector(fetchData), for: .valueChanged)
        refreshController.tintColor = UIColor.darkGray
        return refreshController
    }()
    
    convenience init(presenter: CustomerRecordsPresenter) {
        self.init()
        self.presenter = presenter
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Setting Navbar title
        self.title = "Customers"
        
        //Configuring tableView
        tableView.register(UINib(nibName: String(describing: CustomerCell.self), bundle: Bundle.main), forCellReuseIdentifier: kCELLID_CUSTOMER)
        tableView.refreshControl = refreshControl
        tableView.contentInset.bottom = BOTTOM_SAFE_PADDING
        
        //Attach to presenter and fetch data
        presenter.attach(view: self)
        fetchData()
    }

    @objc func fetchData() {
        presenter.getNearby()
    }

}

extension CustomerRecordsVC: CustomerRecordsViewable {
    func showLoading() {
        refreshControl.beginRefreshing()
    }
    
    func hideLoading() {
        refreshControl.endRefreshing()
    }
    
    func set(customers: [CustomerRecordViewModel]) {
        lblNodata.isHidden = customers.count != 0
        self.dataSource = customers
        self.tableView.reloadData()
    }
    
    func showError(error: Error) {
        showAlert(title: "Error", message: error.localizedDescription)
    }
    
}


extension CustomerRecordsVC : UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kCELLID_CUSTOMER) as! CustomerCell
        cell.customerViewModel = dataSource[indexPath.row]
        return cell
    }
    
}
