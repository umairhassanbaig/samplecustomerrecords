//
//  DataConstants.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/13/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

enum DataFileName: String {
    case customers = "customers"
    
    //testing purpose
    case customers_test1 = "customers_test1"
    case customers_test2 = "customers_test2"
    case customers_test3 = "customers_test3"

}

let kDATAFILES_EXTENSION = "txt"
let kDATAFILES_DELIMETER = "\n"

