//
//  CustomerInteractor.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/13/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit


protocol CustomerInteractorDelegate: class {
    func received(customers: [Customer])
    func failedToReceiveCustomers(error: Error)
}

class CustomerInteractor: BaseInteractor {

    public weak var delegate: CustomerInteractorDelegate?
    
    public func getCustomRecords() {
        getModelsArrayFrom(fileName: .customers, modelType: Customer.self) { (customersArray, error) in
            if error == nil {
                //If error is nil then the array will never be nil
                self.delegate?.received(customers: customersArray!)
            } else {
                self.delegate?.failedToReceiveCustomers(error: error!)
            }
            
        }
        
    }
    
}
