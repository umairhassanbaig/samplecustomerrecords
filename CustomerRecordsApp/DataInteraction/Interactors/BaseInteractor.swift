//
//  BaseInteractor.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/13/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

enum BaseInteractorError: Error{
    case filePath
    case stringConversion
    case decoding
}


class BaseInteractor: NSObject {
    
    internal let json_decoder = JSONDecoder()
    
    internal func getModelsArrayFrom<T: Decodable>(fileName: DataFileName, modelType: T.Type, allowedPartialResults: Bool = true, completion: @escaping ([T]?, Error?) -> Void){
        
        let errorBlock: (BaseInteractorError) -> Void = { (error) in
            DispatchQueue.main.sync {
                completion(nil, error)
            }
        }
        
        //Executing in background thread to avoid delay on main/ui thread
        DispatchQueue.global(qos: .background).async {
            //Initializing file reader
            guard let filePath = Bundle.main.path(forResource: fileName.rawValue , ofType: kDATAFILES_EXTENSION), let fileReader = FileReader(fileURL: URL(fileURLWithPath: filePath), delimeter: kDATAFILES_DELIMETER) else {
                errorBlock(.filePath)
                return
            }
            
            //Repeat until next line is empty. Currently responding with error if any conversion fails
            var resultantArray = [T]()
            repeat {
                if let line = fileReader.readLine() {
                    
                    //Converting string line to data
                    guard let data = line.data(using: .utf8) else {
                        errorBlock(.stringConversion)
                        return
                    }
                    
                    //Parsing into model
                    do {
                        let model = try self.json_decoder.decode(modelType, from: data)
                        resultantArray.append(model)
                    } catch  {
                        if !allowedPartialResults {
                            errorBlock(.decoding)
                            return
                        }
                    }
                    
                } else {
                    //Returning parsed models in main thread
                    DispatchQueue.main.sync {
                        completion(resultantArray, nil)
                    }
                    return
                }
                
            } while true
       }
        
    }
    
    
}

extension BaseInteractorError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .filePath:
            return NSLocalizedString("Interactor failed with invalid file path", comment: "Error")
        case .stringConversion:
            return NSLocalizedString("Interactor failed on string conversion.", comment: "Error")
        case .decoding:
            return NSLocalizedString("Interactor failed on decoding.", comment: "Error")
        }
    }
}
