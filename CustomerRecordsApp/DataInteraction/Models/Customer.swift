//
//  Customer.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/13/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

class Customer: Codable {

    var user_id : Int
    var name: String
    var latitude: String
    var longitude: String

}
