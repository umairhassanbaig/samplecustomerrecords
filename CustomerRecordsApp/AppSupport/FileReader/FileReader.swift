//
//  FileReader.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/13/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

class FileReader {
    
    let encoding: String.Encoding
    let chunkSize: Int
    private let fileHandler: FileHandle
    private let delimPattern : Data
    private var isAtEOF: Bool = false
    private var buffer: Data
    
    init?(fileURL: URL, delimeter: String = "\n", encoding: String.Encoding = .utf8, chunkSize: Int = 4096)
    {
        guard let fileHandler = try? FileHandle(forReadingFrom: fileURL) else {
            return nil
        }
        self.fileHandler = fileHandler
        self.chunkSize = chunkSize
        self.encoding = encoding
        delimPattern = delimeter.data(using: .utf8)!
        buffer = Data(capacity: chunkSize)
    }
    
    public func restart() {
        fileHandler.seek(toFileOffset: 0)
        buffer.removeAll(keepingCapacity: true)
        isAtEOF = false
    }
    
    public func readLine() -> String? {
        if isAtEOF { return nil }
        
        repeat {
            //Checking for range of delimeter
            if let range = buffer.range(of: delimPattern, options: [], in: buffer.startIndex..<buffer.endIndex) {
                //Getting lineData before the delimeter character: range.lowerBound
                let subData = buffer.subdata(in: buffer.startIndex..<range.lowerBound)
                
                let line = String(data: subData, encoding: encoding)
                buffer.replaceSubrange(buffer.startIndex..<range.upperBound, with: [])
                return line
            } else {
                //Reading chunk
                let tempData = fileHandler.readData(ofLength: chunkSize)
                if tempData.count == 0 {
                    isAtEOF = true
                    //if its end of file return the buffer value
                    return (buffer.count > 0) ? String(data: buffer, encoding: encoding) : nil
                }
                buffer.append(tempData)
            }
        } while true
    }
  
    
    deinit {
        fileHandler.closeFile()
    }

}
