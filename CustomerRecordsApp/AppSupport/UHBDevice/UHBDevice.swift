//
//  DeviceConstants.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/14/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit


/*****Use only to determine model w.r.t iphone size*******/


enum UHBDeviceSizeModel {
    case iphone4s
    case iphone5
    case iphone6
    case iphone6Plus
    case iphoneXS
    case iphoneXSMax
    case iphoneXR
    case iPad
    
}

class UHBDevice {
    class var iPhoneModelBySize: UHBDeviceSizeModel {
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                DLOGG("IPHONE 5,5S,5C")
                return .iphone5
            case 1334:
                DLOGG("IPHONE 6,7,8 IPHONE 6S,7S,8S ")
                return .iphone6
            case 1920, 2208:
                DLOGG("IPHONE 6PLUS, 6SPLUS, 7PLUS, 8PLUS")
                return .iphone6Plus
            case 2436:
                DLOGG("IPHONE X, IPHONE XS")
                return .iphoneXS
            case 2688:
                DLOGG("IPHONE XS_MAX")
                return .iphoneXSMax
            case 1792:
                DLOGG("IPHONE XR")
                return .iphoneXR
            default:
                return .iphone4s
            }
        }
        return .iPad
    }
    
}
