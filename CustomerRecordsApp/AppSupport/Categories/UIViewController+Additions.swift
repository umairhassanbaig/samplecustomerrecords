//
//  aaa.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/14/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

extension UIViewController {

    func showAlert(title: String, message: String, buttonTitle: String = "Dismiss", buttonAction: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
            buttonAction?()
        }))
        present(alert, animated: true, completion: nil)
    }

}
