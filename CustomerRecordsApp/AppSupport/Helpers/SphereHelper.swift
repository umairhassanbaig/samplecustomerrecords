//
//  SphereHelper.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/14/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

class SphereHelper {
    
    class func orthodromicDistance(from: Coordinates, to: Coordinates, sphereRadius: Double) -> Double {
        // Using Vincenty formula. Reference https://en.wikipedia.org/wiki/Great-circle_distance
        let phi1 =  toRadians(from.latitude)
        let lemda1 = toRadians(from.longitude)
        
        let phi2 = toRadians(to.latitude)
        let lemda2 = toRadians(to.longitude)
        
        var deltaPhi = phi1 - phi2
        deltaPhi = deltaPhi >= 0 ? deltaPhi : -deltaPhi
        
        var deltaLemda = lemda1 - lemda2
        deltaLemda = deltaLemda >= 0 ? deltaLemda : -deltaLemda
        
        let expression1 = pow( cos(phi2) * sin(deltaLemda), 2)
        let expression2 = pow(cos(phi1)*sin(phi2) - sin(phi1)*cos(phi2)*cos(deltaLemda), 2)
        let expression3 = sin(phi1)*sin(phi2) + cos(phi1)*cos(phi2)*cos(deltaLemda)
        let expression4 = (expression1 + expression2).squareRoot() / expression3
        
        let centralAngle = atan(expression4)
        
        let arcLength = sphereRadius * centralAngle
        // In KM
        return arcLength
    }
    
    
    class func toRadians(_ degrees: Double) -> Double{
        return degrees * .pi / 180
        
    }
    
    class func toDegrees(_ radians: Double) -> Double{
        return radians * 180 / .pi
    }
}
