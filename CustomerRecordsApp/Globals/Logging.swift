//
//  Logging.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/14/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

func DPRINT(items: Any..., separator: String = " ", terminator: String = "\n") {
    #if DEBUG
    var idx = items.startIndex
    let endIdx = items.endIndex
    
    repeat {
        Swift.print(items[idx], separator: separator, terminator: idx == (endIdx - 1) ? terminator : separator)
        idx += 1
    }
        while idx < endIdx
    
    #endif
}

func DLOGG(_ format : String, _ obj : NSObject) {
    #if DEBUG
    NSLog(format,obj);
    #endif
}

func DLOGG(_ string : String) {
    #if DEBUG
    NSLog(string);
    #endif
}
