//
//  Globals.swift
//  CustomerRecordsApp
//
//  Created by Umair Hassan Baig on 3/13/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import UIKit

let kLATITUDE_OFFICE: Double = 53.339322
let kLONGITUDE_OFFICE: Double = -6.257511
let kDISTANCE_CUSTOMER_MAX: Double = 100
let kCOORDINATES_OFFICE = Coordinates(latitude: kLATITUDE_OFFICE, longitude: kLONGITUDE_OFFICE)
let kRADIUS_EARTH: Double = 6371

struct Coordinates {
    var latitude: Double
    var longitude: Double
}




var IS_PHONE_X_SERIES_TYPE_SIZE : Bool {
    let model = UHBDevice.iPhoneModelBySize
    return model == .iphoneXS || model == .iphoneXR || model == .iphoneXSMax
}

private let window = UIApplication.shared.keyWindow
var TOP_SAFE_PADDING : CGFloat {
    let def: CGFloat = IS_PHONE_X_SERIES_TYPE_SIZE ? 34.0 : 20
    if #available(iOS 11.0, *) {
        return window?.safeAreaInsets.top ?? def
    } else {
        return def
    }
}
var BOTTOM_SAFE_PADDING: CGFloat {
    let def: CGFloat = IS_PHONE_X_SERIES_TYPE_SIZE ? 34.0 : 0
    if #available(iOS 11.0, *) {
        return window?.safeAreaInsets.bottom ?? def
    } else {
        return def
    }
}
