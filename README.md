# Sample Customer Records 
## Problem Statement

We have some customer records in a text file (customers.txt) -- one customer per line, JSON lines formatted. This  want to invite any customer within 100km of our office for some food and drinks on us. Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by User ID (ascending).


You can use the first formula from this [Wikipedia article](https://en.wikipedia.org/wiki/Great-circle_distance) to calculate distance. Don't forget, you'll need to convert degrees to radians.



