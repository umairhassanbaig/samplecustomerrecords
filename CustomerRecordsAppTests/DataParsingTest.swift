//
//  CustomerRecordsAppTests.swift
//  CustomerRecordsAppTests
//
//  Created by Umair Hassan Baig on 3/14/19.
//  Copyright © 2019 Umair Hassan Baig. All rights reserved.
//

import XCTest
@testable import CustomerRecordsApp


class DataParsingTest: XCTestCase {

    private let baseInteractor = BaseInteractor()
    override func setUp() {
        let exp = self.expectation(description: "Expecting to complete tests")
        
        exp.perform(#selector(exp.fulfill), with: nil, afterDelay: 6);
        waitForExpectations(timeout: 6, handler: nil);
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPartiallyValidAllowedData() {
        baseInteractor.getModelsArrayFrom(fileName: .customers_test1, modelType: Customer.self, allowedPartialResults: true) { (array, err) in
            XCTAssertTrue(array?.count == 3, "3 rows should be valid")
        }
        
    }
    
    func testPartiallyValidNotAllowedData() {
        baseInteractor.getModelsArrayFrom(fileName: .customers_test1, modelType: Customer.self, allowedPartialResults: false) { (array, err) in
            XCTAssertTrue(err != nil, "Error should be returned in case partialResults are off")
        }
        
    }
    
    func testMultiInsertsData() {
        baseInteractor.getModelsArrayFrom(fileName: .customers_test2, modelType: Customer.self, allowedPartialResults: true) { (array, err) in

            XCTAssertTrue(array?.count == 2, "Array should have 2 values")
        }
        
    }

    func testEmptyData() {
        baseInteractor.getModelsArrayFrom(fileName: .customers_test3, modelType: Customer.self, allowedPartialResults: true) { (array, err) in
            XCTAssertTrue(array?.count == 0, "Array should have 0 values")
        }
        
    }

}
